---
title: "Spenden"
showtoc: true
TocOpen: true
ShowBreadCrumbs: false
---

Eure Spenden machen es möglich. Wir sind eine Gemeinschaft, welche Infrastruktur für sich selbst bereitstellt. Kostenlos und ohne Gewinnabsicht. Wir behalten uns jedoch vor, Spenden ab dem [18. November 2022](https://graz.social/@wir/109365611369405242) auch als Aufwandsentschädigungen für die Moderator*innen und Admins zu verwenden. Etwaige Überschüsse werden als Reserven angelegt, oder können zum Ausbau der Dienste führen. Falls die Dienste eingestellt werden sollten, werden die Finanzreserven in möglichst gemeinschaftlicher Entscheidung an einen Verein, bzw. ein Projekt, oder auch mehrere, mit möglichst ähnlichen Zielen weitergegeben.

## Kosten
Die monatlichen Fixkosten für die Infrastruktur belaufen sich momentan auf:
- 18,67&#8239;€ Hauptserver
- 3,81&#8239;€ für die Domain graz.social
- 3,28&#8239;€ Mail-Hosting && Backup Speicher
- 3,08&#8239;€ Für die Domain graz.events

## Zahlungsarten
Finanziell kann man uns wie folgt unterstützen:
- Überweisung:
```
graz.social - Verein zur Förderung ethischer Digitalkultur
IBAN: AT67 2083 9055 0133 3537
BIC/SWIFT: SPVOAT21XXX
```
- Bar: per Brief an die Adresse im [Impressum](/impressum)
- ~~PayPal~~: Aktuell (Stand 11.1.2024) aufgrund des Wechsels zum Vereinskonto ist PayPal noch nicht eingerichtet.

Teile uns bei deiner Spende gerne ein Zeichen/Verwendungszweck mit, unter der wir deine Spende listen sollen. Ansonsten wird deine Spende lediglich mit Datum und Betrag aufgeführt und ist für dich unter Umständen nicht immer eindeitig auf dich zurück zu führen.

## Transparenz

Der Verein ist bemüht seine Spendeneinkünfte und Ausgaben quartalsweise offen zu legen.

### Bilanz

{{< balance >}}

### Liste aller Eingänge

{{< income >}}

Wöchentliche Spenden via Liberapay werden als einmalige Spenden zum Zeitpunkt der Erneuerung erfasst.

### Liste aller Ausgaben

{{< outcome >}}

## Spendenübersicht bis zur Übernahme durch den Verein
Spenden und Ausgaben vor Gründung des Vereins und Öffnung des Vereinskontos sind [hier](/spendenarchiv) archiviert.
