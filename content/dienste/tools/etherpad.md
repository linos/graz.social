---
title: Etherpad
type: tool
subtitle: "Gemeinsam online Dokumente bearbeiten"
description: "Gemeinsam online Dokumente bearbeiten"
link: "https://pad.graz.social"
weight: 10
tags: ["Tool", "Arbeiten", "Organisieren"]
replaces: ["Google-Docs"]
label:
    name: etherpad
    replacesTitle: true
    height: 40
---

#### Zusammenarbeiten an einem Online-Dokument in Echtzeit.

Kein Hin- und Herschicken von Dokumenten per E-Mail mehr. Richtet einfach ein Pad ein, teilt den Link und los gehts!
Geeignet für Artikel, Pressemitteilungen, To-Do-Listen, oder eine gemeinsame Arbeit an der Uni.

![Bild das die Nutzung von Etherpad zeigt](../images/etherpad_demo.gif)

### Hinweis
Achtung: Die Pads sind nicht verschlüsselt. Jeder, der den Link kennt (oder ihn errät) hat Zugriff.

### Plugins
Etherpad kann von uns durch [(offizielle) Plugins](https://static.etherpad.org/index.html) vielfältig erweitert werden. Wenn du einen Wunsch hast, [kontaktiere uns gerne](mailto:wir@graz.social).

### Weitere Informationen
Auf der Website des Projekts: https://etherpad.org/