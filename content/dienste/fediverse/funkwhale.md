---
title: Funkwhale
type: fediverse
description: "Musik und Podcasts"
link: "https://audio.graz.social"
weight: 7
tags: ["Fediverse", "Soziales-Netzwerk","Podcast", "Musik"]
replaces: ["Soundcloud", "Bandcamp"]
label:
    name: funkwhale
    replacesTitle: true
    height: 33
---

Funkwhale ist ein Gemeinschaftsprojekt, das es dir ermöglicht, Musik und Audio innerhalb eines dezentrales, offenen Netzwerks zu hören und zu teilen.

![Sreenshot der Startseite von Funkwhale](../images/screenshots/funkwhale.jpg)

## Mehr Informationen auf der Seite der Entwickler:
https://funkwhale.audio/

