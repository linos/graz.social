---
title: PeerTube
type: fediverse
link: "https://tube.graz.social"
weight: 2
replaces: ["YouTube", "Vimeo"]
tags: ["Fediverse", "Soziales-Netzwerk", "Video", "Livestream"]
label:
    replacesTitle: true
    name: peertube
    height: 33
---

> Videos und Livestreams

Eine Videostreaming-Plattform findet sich unter [https://tube.graz.social](https://tube.graz.social). Diese beinhaltet nur originale Inhalte, direkt von den UrheberInnen. Zudem listet sie auch Videos von anderen vernetzten PeerTube-Instanzen.
[https://joinpeertube.org/](joinpeertube.org)

## Was ist Peertube
{{< peertube framatube.org "9c9de5e8-0a1e-484a-b099-e80766180a6d" >}}

## Hinweise
Da Videos sehr viel Speicherplatz benötigen ist eine Registrierung nur manuell möglich. Wenn du gerne einen Account hättest, zögere nicht und kontaktiere uns einfach.