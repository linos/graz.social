---
title: Pixelfed
link: "https://pixelfed.graz.social"
type: fediverse
tags: ["Fediverse", "Soziales-Netzwerk", "Fotographie", "Bilder", "Stories"]
weight: 4
replaces: ["Instagram"]
label:
    name: pixelfed
    replacesTitle: true
    height: 42
---

Wenn du im Internet fast ausschließlich Bilder teilen willst und dabei deine Sourveränität behalten willst,
ist eine Pixelfed-Instanz die richtige Plattform für dich.

## Screenshot
![Screenshot eines Pixelfed-Profils](../images/screenshots/pixelfed.webp)

## Features
- Bilder, Galerien und Stories
- Freie Entscheidung darüber, wer welchen Content sehen kann (öffentlich, nur lokal registriete oder nur Folgende)
- Folgen vom ganzen [Fediverse](https://de.wikipedia.org/wiki/Fediverse) aus möglich (z.B. [Mastodon](/dienste/mastodon))
- Well-Being-Mode: zeigt keine Anzahl der Likes und Boosts etc. an.


## Wichtige Hinweise
- Pixelfed befindet sich in der Entwicklung. Die Plattform ist prinzipiell funktional, jedoch werden laufend neue Features eingebaut.
- Die Nutzungbedingungen werden gerade überareitet.

## Registrierung
Wir haben momentan nicht genügend Moderator*innen, um für Pixelfed die selbstständige Registrierung freizuschalten. Hättest du gerne einen Account kontaktiere uns bitte persönlich.