---
title: "Statuten"
ShowBreadCrumbs: true
date: 2023-08-14
editPost:
  disabled: "true"
---

**Präambel**
An Stellen, an denen schriftliche Kommunikation gefordert wird, sind E-Mail sowie weitere geeignete digitale Mittel stets mit eingeschlossen

## § 1 Name, Sitz und Tätigkeitsbereich
1. Der Verein führt den Namen "graz.social – Verein zur Förderung ethischer Digitalkultur".
2. Der Verein hat seinen Sitz in Graz, erstreckt seinen Wirkungsbereich auf ganz Österreich und darüber hinaus, fokussiert diesen jedoch stark auf den Raum Graz und die Steiermark.
3. Das Geschäftsjahr ist das Kalenderjahr.

## §2 Zweck
Der Verein ist nicht auf Gewinn ausgerichtet und verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne der §§ 34ff BAO:
   * Stärkung der Demokratie durch Förderung einer gesunden, freien und möglichst barriere- und werbefreien digitalen Kultur
   * Aufklärung und Bildung zu
      * den Möglichkeiten und Risiken der digitalen Transformation der Gesellschaft
      * Freier Software
      * offenen Protokollen
      * Datensorgsamkeit/Datensparsamkeit
   * Diskussion, Austausch und Vernetzung und Förderung der Verbreitung von vertrauenswürdiger, humaner und datensparsamer
 Protokollen
   * Diskussion, Austausch und Vernetzung und Förderung der Verbreitung von vertrauenswürdiger, humaner und datensparsamer digitaler Dienste (soziale Netzwerke und Online-Tools), welche auf freier Software basieren.


## § 3 Tätigkeiten und Mittel zur Verwirklichung des Vereinszwecks
1. Zur Verwirklichungen des Vereinszwecks sind folgende Mittel vorgesehen:
   * Bereitstellung freier* digitaler Infrastruktur. Die dabei eingesetzte Software muss unter einer Freien Software Lizenz veröffentlicht sein, ethische Design-Prinzipien einhalten und im Hinblick auf Datenschutzsparsamkeit und Datenschutzfreundlichkeit programmiert und betrieben werden. Die bereitgestellte Infrastruktur umfasst zwei Pfeiler:
      * Soziale Netzwerke, bzw. Fediverse Instanzen: Diese ermöglichen es Inhalte zu veröffentlichen, diese zu abonnieren damit zu interagieren (über die Grenzen von graz.social hinaus)
      * Online-Werkzeuge für das tägliche vernetze Arbeiten mit Computern (z.B. Videokonferenzen, Pads, Umfragen, etc.)
   * Entwicklung und Förderung der Entwicklung von freier Software
   * Information der Öffentlichkeit, insbesondere durch Organisation von, oder Teilnahme an:
      * Diskussionen, Tagungen, Workshops oder Vorträgen
      * Publikationen
      * Bildungs- und Weiterbildungsmaßnahmen
   * Zusammenarbeit, Austausch und Förderung nationaler und internationaler Organisationen, deren Ziele mit denen des Vereins vereinbar sind, auch durch gemeinsame Veranstaltungen und Projekte
   * Erarbeitung und Veröffentlichungen von Publikationen in schriftlicher sowie audiovisueller Form
Der Verein ist berechtigt, sich weisungsgebundener Erfüllungsgehilf*innen zu bedienen, sofern auf diese Weise der Vereinszweck besser erreicht werden kann. Der Verein kann auch für andere als Erfüllungsgehilfe tätig werden, sofern dadurch der Vereinszweck besser erreicht werden kann.

2. Die erforderlichen materiellen Mittel werden aufgebracht durch:
   * Beitrittsgebühren
   * Mitgliedsbeiträge
   * Spenden
   * Einnahmen aus Fundraising
   * Einnahmen aus Crowdfunding
   * Bausteinaktionen
   * Vermächtnisse
   * Schenkungen
   * Subventionen und Zuwendungen der öffentlichen Hand
   * Unterstützung durch Privatpersonen und Unternehmen
   * sonstige Zuwendungen
   * Sponsoring
   * Flohmärkte
   * Erträgnisse aus Veranstaltungen und vereinseigenen Unternehmungen
   * Einnahmen aus der Tätigkeit als Erfüllungsgehilfe

## § 4 Arten der Mitgliedschaft
   1. Die Mitglieder des Vereins gliedern sich in ordentliche und außerordentliche Mitglieder.
   1. Ordentliche Mitglieder sind jene, die sich aktiv am Vereinsleben beteiligen. Außerordentliche Mitglieder sind solche, die sich nicht aktiv am Vereinsleben beteiligen, den Verein aber durch Mitgliedsbeiträge unterstützen.

## § 5 Erwerb der Mitgliedschaft
1. Mitglieder des Vereins können alle physischen Personen, sowie juristische Personen und rechtsfähige Personengesellschaften werden.
2. Über die Aufnahme von ordentlichen und außerordentlichen Mitgliedern entscheidet das Leitungsorgan. Die Aufnahme kann ohne Angabe von Gründen verweigert werden.
3. Bis zur Entstehung des Vereins erfolgt die vorläufige Aufnahme von ordentlichen und außerordentlichen Mitgliedern durch die Vereinsgründer*innen, im Fall eines bereits bestellten Leitungsorgans durch dieses. Diese Mitgliedschaft wird erst mit Entstehung des Vereins wirksam. Wird ein Leitungsorgan erst nach Entstehung des Vereins bestellt, erfolgt auch die definitive Aufnahme ordentlicher und außerordentlicher Mitglieder bis dahin durch die Gründer*innen des Vereins.

## § 6 Beendigung der Mitgliedschaft
1. Die Mitgliedschaft erlischt durch Tod, bei juristischen Personen und rechtsfähigen Personengesellschaften durch Verlust der Rechtspersönlichkeit, durch freiwilligen Austritt und durch Ausschluss.
2. Der freiwillige Austritt kann jederzeit erfolgen.
3. Der Ausschluss eines Mitgliedes aus dem Verein kann vom Leitungsorgan auch wegen grober Verletzung anderer Mitgliedspflichten und wegen unehrenhaften Verhaltens verfügt werden.
4. Der Vorstand kann eine Mitgliedschaft auf Antrag des betreffenden Mitglieds für eine dabei festzulegende Dauer auf ruhend setzen. Der Antrag muss den gewünschten Zeitraum der Ruhendstellung enthalten.
5. Eine Ruhendstellung kann auf Antrag beim Vorstand jederzeit wieder aufgehoben werden.

## § 7 Rechte und Pflichten der Mitglieder
1. Die Mitglieder sind berechtigt, an allen Veranstaltungen des Vereines teilzunehmen und die Einrichtungen des Vereins zu beanspruchen. Das Stimmrecht in der Mitgliederversammlung sowie das aktive und passive Wahlrecht stehen nur den ordentlichen zu.
2. Die Mitglieder sind verpflichtet, die Interessen des Vereins nach Kräften zu fördern und alles zu unterlassen, wodurch das Ansehen und der Zweck des Vereins Abbruch erleiden könnten. Sie haben die Vereinsstatuten und die Beschlüsse der Vereinsorgane zu beachten. Die ordentlichen und außerordentlichen Mitglieder sind zur pünktlichen Zahlung der Beitrittsgebühr und der Mitgliedsbeiträge in der von der Mitgliederversammlung beschlossenen Höhe verpflichtet.
3. Ordentliche Mitglieder haben das Recht Einsicht in die Administrations- und Moderationsprozesse der vom Verein betriebenen digitalen Dienste (siehe § 3) zu bekommen. Wenn gewünscht und technisch möglich, ist dieses Recht in Form von entsprechenden Rollen in der betriebenen Software dauerhaft sicherzustellen.

## § 8 Vereinsorgane
Organe des Vereines sind
1. die Mitgliederversammlung (siehe § 9 und § 10)
2. das Leitungsorgan (siehe § 11 bis § 13)
3. die Rechnungsprüfer*innen (siehe § 14) und
4. das Schiedsgericht (siehe § 15)

## § 9 Die Mitgliederversammlung
1. Die ordentliche Mitgliederversammlung findet jährlich statt.
2. Eine außerordentliche Mitgliederversammlung findet auf Beschluss des Leitungsorgans, der ordentlichen Mitgliederversammlung oder auf schriftlichen begründeten Antrag von mindestens einem Zehntel der Mitglieder oder auf Verlangen der Rechnungsprüfer*innen binnen vier Wochen statt.
3. Sowohl zu den ordentlichen wie auch zu den außerordentlichen Mitgliederversammlungen sind alle Mitglieder mindestens zwei Wochen vor dem Termin schriftlich durch E-Mail an die vom Mitglied zuletzt bekannt gegebene Adresse einzuladen. Die Anberaumung der Mitgliederversammlung hat unter Angabe der vorläufigen Tagesordnung zu erfolgen. Die Einberufung erfolgt durch das Leitungsorgan.
4. Anträge zur Mitgliederversammlung sind mindestens eine Woche vor dem Termin der Mitgliederversammlung beim Leitungsorgan schriftlich einzureichen.
5. Bei der Mitgliederversammlung sind alle Mitglieder teilnahmeberechtigt. Stimmberechtigt sind nur die ordentlichen Mitglieder. Jedes Mitglied hat eine Stimme. Juristische Personen werden durch eine*n Bevollmächtigte*n vertreten. Die Übertragung des Stimmrechtes auf ein anderes Mitglied im Wege einer schriftlichen Bevollmächtigung ist zulässig.
6. Die Mitgliederversammlung ist unabhängig von der Anzahl der erschienen stimmberechtigten Mitglieder beschlussfähig.
7. Die Wahlen und die Beschlussfassungen in der Mitgliederversammlung erfolgen in der Regel mit einfacher Stimmenmehrheit.
8. Den Vorsitz in der Mitgliederversammlung führt eine vom Leitungsorgan damit beauftragte Person.

## § 10 Aufgabenkreis der Mitgliederversammlung
Der Mitgliederversammlung sind folgende Aufgaben vorbehalten:
1. Entgegennahme und Genehmigung des Berichts über Tätigkeiten und Finanzgebarung
2. Beschlussfassung über den Voranschlag
3. Wahl, Bestellung und Enthebung der Mitglieder des Leitungsorgans und der Rechnungsprüfer*innen, Genehmigung von Rechtsgeschäften zwischen Mitgliedern des Leitungsorgans oder Rechnungsprüfer*innen mit dem Verein
4. Entlastung des Leitungsorgans
5. Festsetzung der Höhe der Beitrittsgebühr und der Mitgliedsbeiträge für ordentliche und für außerordentliche Mitglieder
6. Beschlussfassung über Statutenänderungen und die freiwillige Auflösung des Vereines
7. Beratung und Beschlussfassung über sonstige auf der Tagesordnung stehende Angelegenheiten

## § 11 Leitungsorgan
1. Das Leitungsorgan besteht aus mindestens zwei Mitgliedern.
2. Das Leitungsorgan wird von der Mitgliederversammlung gewählt. Das Leitungsorgan hat bei Ausscheiden eines gewählten Mitgliedes das Recht, an seine Stelle ein anderes wählbares Mitglied zu kooptieren, wozu die nachträgliche Genehmigung in der nächstfolgenden Mitgliederversammlung einzuholen ist. Fällt das Leitungsorgan ohne Selbstergänzung durch Kooptierung überhaupt oder auf unvorhersehbar lange Zeit aus,  ist jede\_r Rechnungsprüfer\_in verpflichtet, unverzüglich eine außerordentliche Mitgliederversammlung zum Zweck der Neuwahl des Leitungsorgans einzuberufen. Sollten auch die Rechnungsprüfer*innen handlungsunfähig oder nicht vorhanden sein, hat jedes ordentliche Mitglied, das die Notsituation erkennt, unverzüglich die Bestellung einer\_s Kuratorin\_s beim zuständigen Gericht zu beantragen, die\_der umgehend eine außerordentliche Mitgliederversammlung einzuberufen hat.
3. Die Funktionsdauer des Leitungsorgans ist zeitlich unbegrenzt. Wiederwahl ist möglich.
4. Das Leitungsorgan kann von jedem Mitglied des Leitungsorgans einberufen werden.
5. Das Leitungsorgan ist beschlussfähig, wenn alle seine Mitglieder eingeladen wurden und mindestens die Hälfte von ihnen anwesend ist. Besteht das Leitungsorgan nur aus zwei Personen, ist es beschlussfähig, wenn beide Mitglieder anwesend sind.
6. Das Leitungsorgan fasst seine Beschlüsse mit einfacher Stimmenmehrheit, bei Stimmengleichheit gibt die Stimme der\_s Vorsitzenden den Ausschlag. Besteht das Leitungsorgan nur aus zwei Personen oder nehmen nur zwei Mitglieder des Leitungsorgans an der Sitzung des Leitungsorgans teil, so fasst es seine Beschlüsse einstimmig.
7. Den Vorsitz führt das an Jahren älteste anwesende Mitglied des Leitungsorgans.
8. Außer durch den Tod und Ablauf der Funktionsperiode erlischt die Funktion eines Mitglieds des Leitungsorgans durch Enthebung (siehe § 11 Abs. 9) und Rücktritt (siehe § 11 Abs. 10).
9. Die Mitgliederversammlung kann jederzeit das gesamte Leitungsorgan oder einzelne seiner Mitglieder entheben. Die Enthebung tritt mit Bestellung des neuen Leitungsorgans bzw. des neuen Mitglieds des Leitungsorgans in Kraft.
10. Die Mitglieder des Leitungsorgans können jederzeit schriftlich ihren Rücktritt erklären. Die Rücktrittserklärung ist an das Leitungsorgan, im Falle des Rücktrittes des gesamten Leitungsorgans an die Mitgliederversammlung zu richten.

## § 12 Aufgaben des Leitungsorgans
Dem Leitungsorgan obliegt die Leitung und die Führung der laufenden Geschäfte des Vereines. Ihm kommen alle Aufgaben zu, die nicht durch die Statuten einem anderen Vereinsorgan zugewiesen sind. In seinen Wirkungsbereich fallen insbesondere folgende Angelegenheiten:
1. Erstellung des Jahresvoranschlages sowie Abfassung des Berichts über Tätigkeiten und Finanzgebarung im Sinne des Vereinsgesetzes 2002
2. Einberufung und Vorbereitung der ordentlichen und der außerordentlichen Mitgliederversammlung
3. Verwaltung des Vereinsvermögens
4. Aufnahme und Ausschluss von Vereinsmitgliedern
5. Aufnahme und Kündigung von Angestellten des Vereines
6. Das Leitungsorgan kann eine Person mit der Führung der laufenden Geschäfte betrauen, diese ist von in § 13 Abs. 1 genannten Personen mit den notwendigen Vollmachten auszustatten.

## § 13 Vertretung des Vereins nach Außen
1. Jedes Mitglied des Leitungsorgans ist berechtigt, den Verein nach außen zu vertreten (Einzelvertretung).
2. Rechtsgeschäfte zwischen Mitgliedern des Leitungsorgans und dem Verein (Insichgeschäfte) bedürfen zu ihrer Gültigkeit außerdem der Genehmigung eines daran nicht beteiligten Mitglieds des Leitungsorgans. Wenn das Geschäft für alle Mitglieder des Leitungsorgans ein Insichgeschäft darstellt, ist die Zustimmung der Mitgliederversammlung erforderlich.
3. Rechtsgeschäftliche Bevollmächtigungen, den Verein nach außen zu vertreten bzw. für ihn zu zeichnen, können von den in § 13 Abs. 1 genannten Personen erteilt werden.
4. Bei Gefahr im Verzug ist das Leitungsorgan berechtigt, auch in Angelegenheiten, die in den Wirkungsbereich der Mitgliederversammlung fallen, unter eigener Verantwortung selbstständig Anordnungen zu treffen; diese bedürfen jedoch der nachträglichen Genehmigung durch die Mitgliederversammlung.

## § 14 Die Rechnungsprüfung
1. Zwei Rechnungsprüfer*innen werden von der Mitgliederversammlung auf die Dauer von zwei Jahren gewählt. Wiederwahl ist möglich.
2. Den Rechnungsprüfer*innen obliegt die laufende Geschäftskontrolle und die Überprüfung des Rechnungsabschlusses auf die Ordnungsmäßigkeit der Rechnungslegung und die statutengemäße Verwendung der Mittel. Sie haben der Mitgliederversammlung über das Ergebnis der Überprüfung zu berichten.
3. Im Übrigen gelten für die Rechnungsprüfer*innen die Bestimmungen über die Bestellung, die Abwahl und den Rücktritt der Mitglieder des Leitungsorgans sinngemäß (§ 11 Abs. 3, 8, 9 und 10).

## § 15 Das Schiedsgericht
1. Zur Schlichtung von allen aus dem Vereinsverhältnis entstehenden Streitigkeiten ist das vereinsinterne Schiedsgericht berufen.
2. Das Schiedsgericht setzt sich aus drei unbefangenen ordentlichen Vereinsmitgliedern zusammen. Es wird derart gebildet, dass jeder Streitteil dem Leitungsorgan binnen einer Woche ein unbefangenes Mitglied als Schiedsrichter\_in schriftlich namhaft macht. Die beiden namhaft gemachten Schiedsrichter*innen wählen binnen weiterer 14 Tage ein drittes ordentliches Mitglied zur\_m Vorsitzenden des Schiedsgerichtes. Bei Stimmengleichheit entscheidet unter den Vorgeschlagenen das Los. Sollten für die Schiedsrichter*innen und für die\_den Vorsitzende\_n des Schiedsgerichtes keine geeigneten Vereinsmitglieder zur Verfügung stehen, können auch Nichtmitglieder für diese Funktionen namhaft gemacht und gewählt werden.
3. Das Schiedsgericht fällt seine Entscheidung bei Anwesenheit aller seiner Mitglieder mit einfacher Stimmenmehrheit. Es entscheidet nach bestem Wissen und Gewissen. Seine Entscheidungen sind vereinsintern endgültig. Das Schiedsgericht ist kein Schiedsgericht nach den §§ 577 der ZPO (Zivilprozessordnung).

## § 16 Freiwillige Auflösung der Vereins
1. Die freiwillige Auflösung des Vereines kann von der Mitgliederversammlung mit Zweidrittelmehrheit der abgegebenen gültigen Stimmen beschlossen werden.
2. Diese Mitgliederversammlung hat auch – sofern Vereinsvermögen vorhanden ist – über die Abwicklung zu beschließen. Insbesondere hat sie eine\_n Abwickler\_in zu berufen und Beschluss darüber zu fassen, wem diese\_r das nach Abdeckung der Passiva verbleibende Vereinsvermögen zu übertragen hat.
3. Das letzte Leitungsorgan hat die freiwillige Auflösung binnen vier Wochen nach Beschlussfassung der zuständigen Vereinsbehörde schriftlich anzuzeigen.

## § 17 Verwendung des Vereinsvermögens bei ausscheiden von Mitgliedern, bei Auflösung des Vereins oder bei Wegfall der begünstigten Zwecks
Bei Auflösung des Vereins oder bei Wegfall des bisherigen begünstigten Vereinszwecks ist das nach Abdeckung der Passiva verbleibende Vereinsvermögen für gemeinnützige Zwecke im Sinne der §§ 34ff Bundesabgabenordnung (BAO) zu verwenden. Soweit möglich und erlaubt, soll es dabei Institutionen zufallen, die gleiche oder ähnliche Zwecke wie dieser Verein verfolgen.
