---
title: Spendenarchiv
ShowBreadCrumbs: false
---



## Gesamtbilanz bis zur Übernahme der Spenden und Zahlungen durch den Verein

Vor der Vereinsgründung wurde das Projekt maßgebend von André verwaltet und administriert. Die Spenden und alle Rechnungen wurden ebenfalls von André verwaltet. 
Die letzte Bilanz davon:

{{< archiv_balance >}}

Mit dem Überschuss zur Vereinsgründung wurde wie bereits kommuniziert verfahren:

- Spenden ab dem [18. November 2022](https://graz.social/@wir/109365611369405242) wurden als Aufwandentschädigung behalten.
- Das restliche Spendenguthaben in Höhe <span class="label label-success">405,35 €</span> wurde mit 17. Februar 2024 an den Verein überwiesen.


## Transparenzliste der Spenden und Ausgaben bis zur Vereinsgründung
### Liste aller Spenden
Wöchentliche Spenden via Liberapay werden als einmalige Spenden zum Zeitpunkt der Erneuerung erfasst.

{{< archiv_donations >}}

### Liste aller Ausgaben
{{< archiv_expenses >}}




### Archiv vor 2022
Spenden und großteile der Ausgaben vor 2022 sind hier archiviert.

#### Bilanz zum 1.1.2022 00:00 Uhr
|                |          |
|----------------|----------|
| Summe Ausgaben | 503,80 € |
| Summe Spenden  | 704,54 € |
|---------------------------|
| Kontostand     | <span class="label label-success">+200,74€</span> |

#### Monatliche Zusammenfassung

##### Dezember 2021
|                |         |
|----------------|---------|
| Einmalig       |  0,00 € |
| Daueraufträge  |  8,40 € |

##### November 2021
|                |         |
|----------------|---------|
| Einmalig       | 23,00 € |
| Daueraufträge  |  8,40 € |

##### Oktober 2021
|                |         |
|----------------|---------|
| Einmalig       |225,00 € |
| Daueraufträge  |  8,40 € |

##### September 2021
|                |         |
|----------------|---------|
| Einmalig       |  0,00 € |
| Daueraufträge  |  8,40 € |

##### August 2021
|                |         |
|----------------|---------|
| Einmalig       |  0,00 € |
| Daueraufträge  |  8,40 € |


##### Juli 2021
|                |         |
|----------------|---------|
| Einmalig       | 40,00 € |
| Daueraufträge  |  8,40 € |


##### Juni 2021
|                |         |
|----------------|---------|
| Einmalig       | 27,21 € |
| Daueraufträge  |  8,40 € |


##### Mai 2021
|                |         |
|----------------|---------|
| Einmalig       |  0,00 € |
| Daueraufträge  |  5,90 € |


##### April 2021
|                |         |
|----------------|---------|
| Einmalig       |  0,00 € |
| Daueraufträge  |  5,90 € |


##### März 2021
|                |         |
|----------------|---------|
| Einmalig       | 70,00 € |
| Daueraufträge  |  5,90 € |


##### Februar 2021
|                |         |
|----------------|---------|
| Einmalig       | 16,19 € |
| Daueraufträge  |  5,90 € |


##### Jänner 2021
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 5,90 €  |



##### Dezember 2020
|                |         |
|----------------|---------|
| Einmalig       | 50,00 € |
| Daueraufträge  | 3,90 €  |


##### November 2020
|                |         |
|----------------|---------|
| Einmalig       | 86,74 € |
| Daueraufträge  | 3,90 €  |

##### Oktober 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |

##### September 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |


##### August 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |

##### Juli 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |

##### Juni 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |

##### Juni 2020
|                |         |
|----------------|---------|
| Einmalig       | 0,00 €  |
| Daueraufträge  | 3,90 €  |

##### Mai 2020
|                |         |
|----------------|---------|
| Einmalig       | 20,00 € |
| Daueraufträge  | 3,90 €  |

##### April 2020
|                |         |
|----------------|---------|
| Einmalig       | 38,00 € |
| Daueraufträge  | 0,00 €  |
