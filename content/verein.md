---
title: "Verein"
long_title: "graz.social – Verein zur Förderung ethischer Digitalkultur"
showToc: true
TocOpen: false
ShowBreadCrumbs: false
---

## Was wir tun

### Hintergrund
Das Ziel von Firmen wie Facebook und Google ist es Profit zu machen. Diesen machen sie nahezu ausschließlich mit **personalisierter Werbung**, oder genauer gesagt sie treffen Vorhersagen über unser Verhalten und Handeln und schaffen es dieses auch messbar zu beeinflussen. Der Schaden für freie, demokratische Gesellschaften ist enorm, denn Kunden sind unter anderem auch politische Parteien, insbesondere vor Wahlen. Desto mehr Zeit wir auf kommerziellen "sozialen" Plattformen verbringen und dort interagieren, desto besser wird diese Form der Manipulation und umso größer der Profit. Deswegen sind diese Plattformen daraus ausgelegt, dass wir soviel Zeit dort verbringen wie wöglich. In den schlimmsten Fällen mündet dies in Krankheiten, wie beispielsweise Depressionen oder Magersucht.

Diese Art des Wirtschaftens hat auch einen Namen: [Überwachungskapitalismus](https://de.wikipedia.org/wiki/%C3%9Cberwachungskapitalismus). In Summe werden dessen gravierende Auswirkungen auf uns alle jedoch noch nicht richtig eingeordnet, eingenommen von führenden Politiker*innen.

Aufgrund des Geschäftsmodells, das kommerziellen Sozialen Netzwerken inne wohnt, haben wir ebenfalls **kaum Kontrolle darüber, welche Inhalte wir sehen**. Über lange Zeit wird so unsere Persönlichkeit ungeahnt stark beeinflusst, ohne dass wir es merken. Zugleich werden wir bequem und verlernen es selbst Informationen einzuordnen und zu suchen.

### Lösungen
Doch es existieren mittlerweile Alternativen: sie sind **dezentral**, basieren auf **freier Software** und setzen sich ernsthaft mit **ethischen Fragen** auseinander. Das besondere an diesen Alternativen ist, dass diese interoperabel (miteinander verbunden) sind, weil sie alle ein gemeinsames Protokoll verwenden: [ActivityPub](https://en.wikipedia.org/wiki/ActivityPub). Das heißt, jede\*r kann also seinen eigenen Server (genannt Instanz) selbst betreiben, was mittlerweile bereits tausende machen. Bei manchen Server kann sich jede*r registrieren, andere sind für eine bestimme Gruppe gedacht: so stellt mittlerweile beispielsweise die [Europäische Union](https://social.network.europa.eu/about) oder der [deutsche Bund](https://social.bund.de) eigene Server für ihre Behörden bereit, damit diese digital souverän (interaktive) Öffentlichkeitsarbeit leisten können. Die Summe aller Server nennt man das [Fediverse](https://de.wikipedia.org/wiki/Fediverse) (**Fed**erated Un**iverse**).
![Schaubild Fediverse](/dienste/fediverse/images/Fediverse_ActivityPub.png)

#### Wen gibt es neben graz.social noch?
Die folgenden Links bieten eine Übersicht, welche anderen Server, bzw. Instanzen im Fediverse noch so gibt:
- https://joinmastodon.org/communities
- https://fedidb.org/network

#### Links zu mehr Informationen
- [Digitalcourage](https://digitalcourage.de/digitale-selbstverteidigung/fediverse)
- [Kuketz-Blog](https://www.kuketz-blog.de/empfehlungsecke/#fediverse)
- [Fediverse Wiki](https://joinfediverse.wiki/Main_Page/de)

### Ziele
Die auf graz.social verfügbaren Alternativen zu Sozialen Netzwerken sind wichtige Bausteine für eine gedeihliche Entwicklung der Gesellschaft im digitalen Zeitalter. graz.social ist ein Reallabor in unserer Stadt Graz, um die Praxistauglichkeit dieser Plattformen zu demonstrieren, welche für den Menschen da sind und nicht für den Profit. Das langfristige Ziel ist es zu erreichen, dass insbesondere auch staatliche Stellen und Kulturschaffende ebenfalls diese freien Netzwerke nutzen: für ein gestärktes kulturelles Miteinander, ob direkt auf graz.social oder auf eigenen mit graz.social und dem Fediverse verbundenen Instanzen. Zudem sehen wir uns als ein Dreh- und Angelpunkt für Aufklärungs- und Bildungsarbeit in diesem Bereich.

## Personen

### Vorstand
- **Karl Voit** [(@publicvoit)](https://graz.social/@publicvoit) hilft als Moderator bei Mastodon und unterstützt das Projekt seit anbeginn. Bio: [https://karl-voit.at/about/](https://karl-voit.at/about/)
- **Benjamin Witzerstorfer** unterstützt das Projekt seit 2021

### Mithelfer*innen
- **André** ([@linos](https://graz.social/@linos)) Treibt das Projekt seit der ersten Stunde voran. Er ist der Haupt-Server-Admin, kümmert sich um die Website und verwaltet die Spenden und Ausgaben. Neben Sachen mit dem Computer ist er als Musiker und Instrumentalpädagoge tätig.
- **ruru** [(@ruru4143)](https://graz.social/@ruru4143) ist ebenfalls Server-Admin, sodass im Notfall nicht alles an einer Person hängt, betreut aber auch hauptverantwortlich unser Server Monitoring. Er studiert Informatik in Graz und ist außerdem aktiv bei [r3.at](https://r3.at) und [chaos.jetzt](https://chaos.jetzt), bekennender Diskordianer.
- **Matthias** [(@totoroot)](https://graz.social/@totoroot) hat viel Erfahrung mit der Administration von Linux-Servern. Er steht momentan als Ansprechpartner bei technischen Problemen zur Verfügung und hilft ebenfalls bei der Moderation von Mastodon mit.
- **Cosima** hat immer ein offenes Ohr und einen guten Rat für die Entwicklung von graz.social parat und ist uns als Moderatorin von graz.social, inbesondere bei schwierigen Moderationsentscheidungen, eine große Hilfe.

## Statuten
Die Statuten unseres Vereins findet ihr [hier](/statuten).

## Geschichte
Die Idee zu graz.social ist zu Beginn der Corona-Pandemie während eines Telefonates zwischen [André](https://graz.social/@linos) und [Thomas](https://graz.social/@tomgoe) geboren worden.

## Entwicklung

### Offene Treffen
Wir halten alle paar Monate offene Treffen ab. Zusammenfassungen dieser findet ihr [hier](/protokolle).

### Erweiterung der Dienste
Ausblick: mögliche weitere Dienste
- [BookWyrm](https://joinbookwyrm.com/): sich dezentral über Bücher austauschen
- [Gotify](https://unifiedpush.org/users/distributors/gotify/)/[ntfy](https://ntfy.sh/): Push Nachrichten am Handy (Android) ohne Google-Dienste
- [LimeSurvey](https://www.limesurvey.org/de): erstellen von größeren Umfragen
- [EteSync](https://www.etesync.com/): Kalendersynchronisation: Ende-zu-Ende Verschlüsselt
- [Cryptpad](https://cryptpad.fr/): Gemeinsamer-Online-Dokumente-Editor

## Infrastruktur
Wir mieten einen virtuellen Server mit Standort in Nürnberg (bei der [netcup GmbH](https://www.netcup.de/)). Backups von den Datenbanken erfolgen täglich auf ein nicht öffentliches Webhosting, welches ebenfalls bei netcup gehostet wird. Wöchentlich werden die Datenbanken, Medien und Konfigurationsdateien auf einer verschlüsselten Festplatte in Andrés Wohnung gesichert.
