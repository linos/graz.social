---
title: "Offenes Treffen"
summary: "Bericht - Offene Runde - Standort des Servers - Dienste - Moderation"
date: "2022-05-22"
---

## Bericht
Zahlen und Fakten zum **Finanziellen** und zu den **Server-Ressourcen/Umstieg**
* [https://info.graz.social/spenden/](https://info.graz.social/spenden/)
* Spende der Grazer Linuxtage ermöglicht entspannte finanzielle Lage zur Zeit
* Ausblick: Welche Dienste sollen (langfristig) angeboten werden? Für wen? Zu welchen Bedingungen?

## Offene Runde
* Recht wenig Instanzen aus/für Österreich* u.a. [http://wien.rocks/](http://wien.rocks/)
* Vereinsgründung: abfragen der Bereitsschaft zum Mitgründen
* Diskussion zu den **Serverregeln** und deren Umsetzung (Moderation)
* GMail ist nun frei, da weniger Bots im Verhältnis.
* Musk-Twitter-Ansturm scheint wieder abgeebbt zu sein.
* Regeln und Filterlisten sollten in Git-Repos wandern: für mehr Transparenz.
* Servermigration verschoben auf die kommenden Wochen
  * Hauptgrund: Festplattenspeicher (aktuell 40GB, nur mastodon-medien, insgesamt 140GB all inkl.)

### Standort des Servers
* Vermutlich ist Deutschland gleich gut/schnell wie Ö aber mehr Angebot bzw. besserer Preis.
* Aktuell: 1 virtuelle Maschine bei netcup5 mit viel RAM und wenig HDD und 8 CPU-Kernen

### Welche Services sollten angeboten werden?
* André: eher mehrere Services mit in Summe breiten Anwendungsbereichen, aber nicht nur ein Service mit breitem Spektrum in einer zentralen Oberfläche.
* André ist offen für weitere Dienste: Lemmy (reddit-Klon), Bookworm (Bücherrezensionen),

### Moderation
Parteien oder Firmen, die sich registrieren: wie damit umgehen?
* Aktuell: erlaubt aber Zustimmung von André erforderlich
* Was ist für uns zu kommerziell? -> Man könnte für die Beurteilung Aspekte wie Frequenz, Kommunikationsstil (Werbung vs. Information) und Dark Patterns heranziehen. Beispiel: Der Menüplan eines Gasthauses 1× pro Woche wäre nach diesen Maßstäben als harmlos zu beurteilen.

### Weitere Fragen
* Ein österreichisches **Bankkonto** würde Überweisungen deutlich vereinfachen.
* **Was passiert mit Toots**, die man bookmarket? Bleiben die lokal gesichert solange man User ist? Was passiert wenn der Beitrag vom Ersteller gelöscht wird? -> Dann bekommt auch unser Server eine Löschanfrage, welcher er auch nachkommt!
* Jemand kann bei **Pixelfed** keine Beiträge erstellen. Weder im webbrowser noch mit der Fedilab ab.








